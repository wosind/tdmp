const globalData = {};

/**
 * 
 * @param {*} key 
 * @param {*} val 
 */
export function setGlobalData(key, val) {
  globalData[key] = val;
}

/**
 * 
 * @param {*} key 
 * @param {*} isDelete 
 */
export function getGlobalData(key, isDelete = false) {
  const t = globalData[key];
  if (isDelete) {
    delete globalData[key];
  }
  return t;
}

/**
 * 
 * @param {*} key 
 */
export function removeGlobalData(key) {
  delete globalData[key];
}
