/**
 * 全局缓存，global data
 */
export const wxData = {};

/**
 * 登录用户基本信息
 */
export const CacheUserInfo = 'CacheUserInfo';

/**
 *
 */
export const CacheCookies = 'CacheCookies';

export const CacheLastProductId = 'CacheLastProductId';

export const CacheLastLoginUserName = 'CacheLastLoginUserName';
export const CacheLastLoginPassword = 'CacheLastLoginPassword';
export const CacheLastLoginDate = 'CacheLastLoginDate';

export const CacheTempProductId = 'CacheTempProductId';

export const CacheTemp = 'CacheTemp';

/**
 * App启动路由
 */
export const CacheAppStartRouter = 'CacheAppStartRouter';
