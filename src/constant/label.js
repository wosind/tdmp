export const priObject = {
  0: { label: '', color: '#838a9d' },
  5: { label: '急(3d)', color: '#ff9800' },
  3: { label: '中(15d)', color: '#2098ee' },
  1: { label: '紧急(24h)', color: '#d50000' },
  2: { label: '高(7d)', color: '#838a9d' },
  4: { label: '低(30d)', color: '#838a9d' }
};
export const priArray = [['', '急(3d)', '中(15d)', '紧急(24h)', '高(7d)', '低(30d)']];
export const priValueArray = ['0', '5', '3', '1', '2', '4'];
export const bugStatus = {
  active: { label: '激活', color: '#8666b8' },
  resolved: { label: '已解决', color: '#43a047' },
  closed: { label: '已关闭', color: '#838a9d' }
};
export const bugTypesArray = [
  [
    '代码错误',
    '配置相关',
    '安装部署',
    '安全相关',
    '性能问题',
    '标准规范',
    '测试脚本',
    '其他',
    '设计缺陷',
    '用户体验优化',
    '需求缺陷',
    '新需求',
    '界面优化'
  ]
];
export const bugTypesValueArray = [
  'codeerror',
  'config',
  'install',
  'security',
  'performance',
  'standard',
  'automation',
  'others',
  'designdefect',
  '1',
  '2',
  '3',
  'interface'
];
export const resolutionObject = {
  notbug: '配置问题',
  bydesign: '设计如此',
  duplicate: '重复Bug',
  external: '外部原因',
  fixed: '已解决',
  notrepro: '无法重现',
  postponed: '延期处理',
  willnotfix: '不予解决',
  tostory: '转为需求'
};

export const taskStatus = {
  null: '',
  wait: { label: '未开始', color: '#838a9d' },
  doing: { label: '进行中', color: '#ff5d5d' },
  done: { label: '已完成', color: '#43a047' },
  pause: { label: '已暂停', color: '#838a9d' },
  cancel: { label: '已取消', color: '#838a9d' },
  closed: { label: '已关闭', color: '#838a9d' }
};
export const taskPriObject = {
  0: { label: '0', color: '#838a9d' },
  1: { label: '1', color: '#d50000' },
  2: { label: '2', color: '#ff9800' },
  3: { label: '3', color: '#2098ee' },
  4: { label: '4', color: '#009688' },
  5: { label: '5', color: '#838a9d' }
};
