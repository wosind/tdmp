import { Button, OpenData, View } from '@tarojs/components';
import {
  navigateTo,
  showModal,
  showToast,
  useDidShow,
  useShareAppMessage,
  useState,
  removeStorageSync
} from '@tarojs/taro';
import icon_help from '../../assets/page/icon_help@2x.png';
import icon_yijian from '../../assets/page/icon_yijian@2x.png';
import icon_check from '../../assets/page/icon_check@2x.png';
import Layout from '../../components/Layout';
import XIcon from '../../components/XIcon';
import XList from '../../components/XList';
import {
  CacheUserInfo,
  CacheCookies,
  CacheLastLoginDate,
  CacheLastLoginPassword
} from '../../constant/cache';
import { AppName } from '../../constant/config';
import { getCache } from '../../utils/utils';

const Index = () => {
  const [user, setUser] = useState({});
  const status = {
    dev: '研发',
    qa: '测试',
    pm: '项目经理',
    po: '产品经理',
    td: '研发主管',
    pd: '产品主管',
    qd: '测试主管',
    top: '高层管理',
    others: ' 其他'
  };

  useShareAppMessage((res) => {
    return {
      title: AppName,
      path: '/pages/workb/index'
    };
  });

  useDidShow(() => {
    getUserInfo();
  });

  const getUserInfo = () => {
    const user = getCache(CacheUserInfo) || {};
    setUser(user);
  };

  const onRedirect = (url) => {
    navigateTo({ url });
  };

  const onLogout = () => {
    showModal({ title: '操作提示', content: '确定要退出吗' }).then(({ confirm }) => {
      if (confirm) {
        removeStorageSync(CacheCookies);
        removeStorageSync(CacheLastLoginDate);
        removeStorageSync(CacheLastLoginPassword);
        showToast({ title: '退出成功' });
        navigateTo({ url: '/pages/auth/login' });
      }
    });
  };

  return (
    <Layout>
      <View
        style={{
          padding: '150px 10px 30px 15px',
          background: '#fff',
          marginBottom: '10px',
          display: 'flex'
        }}
      >
        <View
          style={{
            borderRadius: '50%',
            overflow: 'hidden',
            width: '80px',
            height: '80px',
            zIndex: 10000
          }}
        >
          <OpenData type="userAvatarUrl"></OpenData>
        </View>
        <View
          style={{
            flex: 1,
            paddingLeft: '30px',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-around'
          }}
        >
          <View style={{ fontSize: '18px', fontWeight: 800 }}>
            {user['realname'] || <OpenData type="userNickName"></OpenData>}
          </View>
          <View>
            {user['company']} {status[user['role']]}
          </View>
          <View style={{ fontSize: '12px' }}>最近登录：{user['last']}</View>
        </View>
      </View>
      <XList
        renderIcon={<XIcon src={icon_check}></XIcon>}
        arrow="right"
        title="搜索bug"
        onClick={onRedirect.bind(this, '/pages/workb/search')}
      ></XList>
      <XList
        renderIcon={<XIcon src={icon_check}></XIcon>}
        arrow="right"
        title="搜索task"
        onClick={onRedirect.bind(this, '/pages/rd/search')}
      ></XList>
      <XList renderIcon={<XIcon src={icon_yijian}></XIcon>} arrow="right" isCustomize={true}>
        <Button
          openType="share"
          style={{
            padding: 0,
            margin: 0,
            fontSize: 'initial',
            background: 'initial',
            color: 'initial',
            lineHeight: 'initial',
            textAlign: 'left'
          }}
        >
          分享给同事
        </Button>
      </XList>
      <XList isCustomize={true} onClick={onLogout} style={{ marginTop: '30px' }}>
        <View className="button" style={{ textAlign: 'center' }}>
          退出登录
        </View>
      </XList>
    </Layout>
  );
};

Index.config = {
  navigationBarTitleText: '我的',
  navigationStyle: 'custom',
  disableScroll: true
};
