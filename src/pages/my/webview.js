import { WebView } from '@tarojs/components';
import { useRouter } from '@tarojs/taro';

const Web = () => {
  const {
    params: { url }
  } = useRouter();
  return <WebView src={url}></WebView>;
};
export default Web;
