import { View, Picker, Block } from '@tarojs/components';
import {
  showToast,
  useDidShow,
  useState,
  switchTab,
  setStorageSync,
  showLoading,
  hideLoading,
  getStorageSync
} from '@tarojs/taro';
import Layout from '../../components/Layout';
import baseService from '../../utils/baseService';

const Edit = () => {
  const [user, setUser] = useState({});
  const [loading, setLoading] = useState(false);
  const [patients, setPatients] = useState([]);
  const [searchKey, setSearchKey] = useState('');
  const [patientId, setPatientId] = useState('');
  const [isForce, setIsForce] = useState(false);
  useDidShow(() => {});

  return (
    <Layout>
      <View></View>
    </Layout>
  );
};
Edit.config = { navigationBarTitleText: '编辑资料' };
export default Edit;
