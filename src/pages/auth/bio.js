import { Block, Button, Icon } from '@tarojs/components';
import { navigateBack, redirectTo, useRouter, useState } from '@tarojs/taro';
import BioAuth from '../../components/BioAuth';
import Layout from '../../components/Layout';
import Result from '../../components/Result';
import XList from '../../components/XList';

const Bio = () => {
  const [isAuthOk, setIsAuthOk] = useState(null);
  const [message, setMessage] = useState('');
  const {
    params: { url = '' }
  } = useRouter();

  const onAuth = (status, message) => {
    if (status === 1) {
      setIsAuthOk(true);
      redirectTo({ url: decodeURIComponent(url) });
    } else {
      setIsAuthOk(false);
      setMessage(message);
    }
  };

  const onBack = () => {
    navigateBack({ delta: 1 });
  };

  return (
    <Layout isShowHeader={true}>
      <XList isCustomize={true}>
        {isAuthOk === false ? (
          <Block>
            <Result
              message={message}
              renderIcon={<Icon size="50" type="warn" color="red"></Icon>}
            ></Result>
            <Button type="primary" onClick={onBack}>
              返回
            </Button>
          </Block>
        ) : (
          <BioAuth onAuth={onAuth}></BioAuth>
        )}
      </XList>
    </Layout>
  );
};

Bio.config = {
  navigationBarTitleText: '生物认证',
  navigationStyle: 'custom'
};

export default Bio;
