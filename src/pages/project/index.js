import { View } from '@tarojs/components';
import { hideLoading, showLoading, switchTab, useDidShow, useState } from '@tarojs/taro';
import icon_title from '../../assets/page/icon_title@2x.png';
import Layout from '../../components/Layout';
import XIcon from '../../components/XIcon';
import XList from '../../components/XList';
import XRow from '../../components/XRow';
import { CacheTempProductId } from '../../constant/cache';
import baseService from '../../utils/baseService';
import { setCache } from '../../utils/utils';

const Index = () => {
  const [rows, setRows] = useState([]);
  const [lines, setLines] = useState({});

  useDidShow(() => {
    getProducts();
  });

  const getProducts = () => {
    showLoading({ title: '操作中' });
    baseService
      .query('/product-all---noclosed---100.json', {})
      .then(({ data = {} }) => {
        const { productStats = [], lines = {} } = data;
        console.log(data);
        setRows(productStats);
        setLines(lines);
        hideLoading();
      });
  };

  const onRedirect = (url, pid) => {
    setCache(CacheTempProductId, pid);
    switchTab({ url });
  };

  return (
    <Layout>
      <XList
        title="我的项目"
        renderIcon={<XIcon src={icon_title} style={{ width: '16px', height: '16px' }}></XIcon>}
      ></XList>
      {rows.map((x) => (
        <XList
          arrow="right"
          isCustomize={true}
          key={x.id}
          style={{ padding: '8px 10px 8px 15px', marginBottom: '4px', fontSize: '14px' }}
          onClick={onRedirect.bind(this, '/pages/workb/index', x.id)}
        >
          <XRow>{x['name']}</XRow>
          <XRow style={{ fontSize: '12px', color: '#8c8c8c' }}>
            <View style={{ display: 'flex' }}>
              <View style={{ marginRight: '5px' }}>产品线：{lines[x['line']]}</View>
              <View style={{ marginRight: '5px' }}>需求数：{x['stories']['active'] || 0}</View>
              <View style={{ marginRight: '5px' }}>
                bug：
                {parseInt(x['unResolved'] || 0) +
                  parseInt(x['assignToNull'] || 0) +
                  parseInt(x['bugs'] || 0)}
              </View>
            </View>
          </XRow>
        </XList>
      ))}
    </Layout>
  );
};
Index.config = { navigationBarTitleText: '项目' };
export default Index;
