import { Button, Text, View } from '@tarojs/components';
import {
  downloadFile,
  hideLoading,
  navigateTo,
  openDocument,
  showLoading,
  showModal,
  showToast,
  useEffect,
  useRouter,
  useShareAppMessage,
  useState
} from '@tarojs/taro';
import Layout from '../../components/Layout';
import XFilter from '../../components/XFilter';
import XList from '../../components/XList';
import XRow from '../../components/XRow';
import { CacheTemp } from '../../constant/cache';
import { bugStatus, priObject } from '../../constant/label';
import baseService from '../../utils/baseService';
import { formatFile, formatHtml, setCache } from '../../utils/utils';
import Historys from './components/Historys';

const Detail = () => {
  const [curr, setCurr] = useState({ bug: {}, actions: [], users: {} });
  const [files, setFiles] = useState([]);
  const {
    params: { id }
  } = useRouter();

  useEffect(() => {
    getRow();
  }, []);

  useShareAppMessage((res) => {
    return {
      title: curr['title'],
      path: '/pages/workb/detail?id=' + curr['bug']['id']
    };
  });

  const getRow = () => {
    showLoading({ title: '操作中' });
    baseService.query('/bug-view-' + id + '-.json', {}).then(({ data = {} }) => {
      console.log(data);
      hideLoading();
      if (Object.keys(data).length) {
        const bg = data.bug;
        const pics = [];
        const files = [];
        if (data.bug.files && Object.keys(bg.files).length) {
          Object.keys(bg.files).forEach((x) => {
            if (['png', 'jpg', 'jpeg', 'bmp', 'gif', 'webp'].includes(bg.files[x].extension)) {
              pics.push(formatHtml(`<img src="/file-read-${x}.html" />`));
            } else {
              files.push({
                title: bg.files[x].title,
                ext: bg.files[x].extension,
                url: formatFile(`/file-read-${x}.html`)
              });
            }
          });
        }

        data.bug.steps = formatHtml(bg.steps);
        data.bug.attachment = pics.join('<br />');
        setFiles(files);
        setCurr(data);
      } else {
        showToast({ title: '不存在的bug，或者无权限访问', icon: 'none' });
      }
    });
  };

  const onRedirect = (url) => {
    setCache(CacheTemp, curr);
    navigateTo({ url });
  };

  const onClose = () => {
    showModal({
      title: '操作提示',
      content: '确定要关闭吗'
    }).then(({ confirm }) => {
      if (confirm) {
        baseService
          .post(
            `/bug-close-${curr['bug']['id']}.json`,
            {
              status: 'closed',
              comment: '来自oa系统的关闭操作'
            },
            { isForm: true }
          )
          .then((res) => {
            console.log(res);
            getRow();
            showToast({ title: '关闭成功' });
          });
      }
    });
  };

  const onActive = () => {
    showModal({
      title: '操作提示',
      content: '确定要激活吗'
    }).then(({ confirm }) => {
      if (confirm) {
        baseService
          .post(
            `/bug-activate-${curr['bug']['id']}.json`,
            {
              status: 'active',
              comment: '来自oa系统的激活操作'
            },
            { isForm: true }
          )
          .then((res) => {
            console.log(res);
            getRow();
            showToast({ title: '激活成功' });
          });
      }
    });
  };

  const onResolved = () => {
    showModal({
      title: '操作提示',
      content: '确定要解决吗'
    }).then(({ confirm }) => {
      if (confirm) {
        baseService
          .post(
            `/bug-resolve-${curr['bug']['id']}.json`,
            {
              resolution: 'fixed',
              resolvedBuild: 'trunk',
              status: 'resolved',
              comment: '来自oa系统的解决操作'
            },
            { isForm: true }
          )
          .then((res) => {
            console.log(res);
            getRow();
            showToast({ title: '操作成功' });
          });
      }
    });
  };

  const onChangeUser = (groupIndex, value, index) => {
    const userName = Object.keys(curr['users'])[index];
    baseService
      .post(
        `/bug-assignTo-${curr['bug']['id']}.json`,
        {
          assignedTo: userName,
          comment: '来自oa系统的指派操作'
        },
        { isForm: true }
      )
      .then((res) => {
        console.log(res);
        getRow();
        showToast({ title: '操作成功' });
      });
  };

  const onDelete = () => {
    showModal({
      title: '操作提示',
      content: '确定要删除吗'
    }).then(({ confirm }) => {
      if (confirm) {
        baseService
          .query(`/bug-delete-${curr['bug']['id']}-yes.json`, {})
          .then((res) => {
            console.log(res);
            getRow();
            showToast({ title: '删除成功' });
          });
      }
    });
  };

  const onOpen = ({ url, ext }) => {
    showLoading({ title: '操作中' });
    downloadFile({
      url,
      success: function(res) {
        const filePath = res.tempFilePath;
        openDocument({
          filePath: filePath,
          fileType: ext,
          success: function(res) {
            hideLoading();
          },
          fail({ errMsg }) {
            showToast({ title: '打开文件失败,' + errMsg, icon: 'none' });
          }
        });
      },
      fail({ errMsg }) {
        showToast({ title: '下载文件失败,' + errMsg, icon: 'none' });
      }
    });
  };

  const onComment = () => {
    setCache(CacheTemp, curr);
    navigateTo({ url: 'comment?id=' + id });
  };

  return (
    <Layout>
      <View style={{ paddingBottom: 'calc(50px + env(safe-area-inset-bottom))' }}>
        <XList isCustomize={true}>
          <View style={{ display: 'flex', flexWrap: 'wrap' }}>
            <Text decode={true}> {curr['title']}</Text>
          </View>
          <XRow style={{ fontSize: '12px', color: '#8c8c8c' }}>
            <XRow>
              <View
                style={{
                  marginRight: '5px'
                }}
              >
                #{curr['bug']['id']}
              </View>
              <View
                style={{
                  color: (bugStatus[curr['bug']['status']] || {}).color,
                  marginRight: '5px'
                }}
              >
                {(bugStatus[curr['bug']['status']] || {}).label}
              </View>
              <View
                style={{ color: (priObject[curr['bug']['pri']] || {}).color, marginRight: '5px' }}
              >
                {(priObject[curr['bug']['pri']] || {}).label}
              </View>
            </XRow>
          </XRow>
        </XList>
        <XList isCustomize={true}>
          <XRow>
            <View style={{ display: 'flex', alignItems: 'center' }}>
              <View style={{ display: 'flex', alignItems: 'center', marginRight: '15px' }}>
                <Button
                  type="primary"
                  size="mini"
                  onClick={onRedirect.bind(this, 'edit?id=' + curr['bug']['id'])}
                >
                  修改
                </Button>
              </View>
              <XFilter
                filterOptions={[Object.values(curr.users)]}
                defaultLabel="指派给"
                onChange={onChangeUser}
                valueStyle={{
                  height: '29px',
                  padding: '0 6px',
                  borderRadius: '2px',
                  color: '#333',
                  background: '#d5d5d5',
                  fontSize: '12px'
                }}
              ></XFilter>
            </View>
            <View style={{ display: 'flex', alignItems: 'center' }}>
              {curr['bug']['deleted'] === '0' && (
                <View style={{ marginRight: '15px' }}>
                  <Button type="default" size="mini" onClick={onDelete}>
                    删除
                  </Button>
                </View>
              )}
              {curr['bug']['status'] === 'resolved' && (
                <View style={{ marginRight: '15px' }}>
                  <Button type="warn" size="mini" onClick={onClose}>
                    关闭
                  </Button>
                </View>
              )}
              {curr['bug']['status'] === 'closed' && (
                <View style={{ marginRight: '15px' }}>
                  <Button type="warn" size="mini" onClick={onActive}>
                    激活
                  </Button>
                </View>
              )}
              {curr['bug']['status'] === 'active' && (
                <View style={{ marginRight: '15px' }}>
                  <Button type="warn" size="mini" onClick={onResolved}>
                    解决
                  </Button>
                </View>
              )}
            </View>
          </XRow>
        </XList>
        <XList isCustomize={true}>
          <wxparser rich-text={curr['bug']['steps'] || ''} />
          {curr['bug']['attachment'] && (
            <wxparser rich-text={'<br />[附件]<br />' + curr['bug']['attachment']} />
          )}
          {files.length > 0 && (
            <View>
              [文件附件]
              {files.map((x) => (
                <View key={x.url} onClick={onOpen.bind(this, x)} style={{ color: '#1890ff' }}>
                  {x.title}
                </View>
              ))}
            </View>
          )}
        </XList>
        <XList isCustomize={true}>
          <Historys users={curr.users} actions={Object.values(curr.actions)}></Historys>
        </XList>
      </View>

      <View
        style={{
          position: 'fixed',
          bottom: 0,
          marginBottom: 0,
          width: '100%',
          background: '#23d1a5',
          color: '#fff',
          zIndex: 10000,
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          paddingTop: '14px',
          paddingBottom: 'calc(14px + env(safe-area-inset-bottom))'
        }}
        onClick={onComment}
      >
        添加BUG备注
      </View>
    </Layout>
  );
};
Detail.config = {
  navigationBarTitleText: 'Bug详情',
  usingComponents: {
    wxparser: 'plugin://wxparserPlugin/wxparser'
  }
};
export default Detail;
