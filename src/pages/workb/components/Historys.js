import { View, Block } from '@tarojs/components';
import XRow from '../../../components/XRow';
import { formatHtml } from '../../../utils/utils';
import { resolutionObject } from '../../../constant/label';

const Historys = ({ actions, users }) => {
  const action = {
    opened: '创建',
    assigned: '指派给',
    bugconfirmed: '确认Bug',
    resolved: '解决',
    activated: '激活',
    closed: '关闭',
    commented: '添加备注',
    edited: '编辑',
    deleted: '删除',
    gitcommited: '提交代码'
  };

  const renderComment = (comment) =>
    comment ? (
      <View style="width: 100%;background: #f0f0f0;padding: 5px;margin-left: 15px;border: 1px solid #ccc;margin-top: 5px;overflow:auto;max-width:calc(100vw - 45px)">
        <wxparser rich-text={formatHtml(comment)} />
      </View>
    ) : (
      ''
    );

  const renderStrong = (str) => <View style="font-weight: 800;margin: 0 2px">{str}</View>;

  return (
    <View style={{ fontSize: '12px' }}>
      {actions.map((x, xIndex) => (
        <View key={x.id} style={{ display: 'flex', flexDirection: 'column', padding: '3px 0' }}>
          <View style={{ display: 'flex' }}>
            <View>
              {xIndex + 1}. {x.date}，
            </View>
            {[
              'opened',
              'closed',
              'bugconfirmed',
              'activated',
              'commented',
              'edited',
              'deleted'
            ].includes(x.action) ? (
              <View style={{ display: 'flex' }}>
                由{renderStrong(users[x.actor] || x.actor)}
                {action[x.action]}
                {x.extra ? <View>，方案为{renderStrong(resolutionObject[x.extra])}</View> : null}。
              </View>
            ) : x.action === 'gitcommited' ? (
              <View style={{ display: 'flex' }}>
                由{renderStrong(users[x.actor] || x.actor)}
                {action[x.action]}。
              </View>
            ) : x.action === 'assigned' ? (
              <View style={{ display: 'flex' }}>
                由{renderStrong(users[x.actor] || x.actor)}
                {action[x.action]}
                {renderStrong(users[x.extra] || x.actor)}。
              </View>
            ) : x.action === 'resolved' ? (
              <View style={{ display: 'flex' }}>
                由{renderStrong(users[x.actor] || x.actor)}
                {action[x.action]}，方案为{renderStrong(resolutionObject[x.extra])}。
              </View>
            ) : (
              <View></View>
            )}
          </View>
          {renderComment(x.comment)}
        </View>
      ))}
    </View>
  );
};
Historys.config = {
  usingComponents: {
    wxparser: 'plugin://wxparserPlugin/wxparser'
  }
};
Historys.defaultProps = {
  actions: [],
  users: {}
};
export default Historys;
