import Layout from '../../components/Layout';
import XList from '../../components/XList';
import XInput from '../../components/XInput';
import { View } from '@tarojs/components';
import { useState, navigateTo, showToast } from '@tarojs/taro';

const Search = () => {
  const [key, setKey] = useState('');

  const onChange = (e) => {
    setKey(e.detail.value);
  };
  const onSearch = () => {
    if (key && parseInt(key) > 0) {
      navigateTo({ url: 'detail?id=' + key });
    } else {
      showToast({ title: '不支持的查询', icon: 'none' });
    }
  };

  return (
    <Layout isShowHeader={true} headerStyle={{ marginTop: '40px' }}>
      <XList isCustomize={true}>
        <XInput
          label="task编号"
          placeholder="输入task ID"
          type="number"
          focus={true}
          onChange={onChange}
        ></XInput>
      </XList>
      <XList isCustomize={true} onClick={onSearch} style={{ marginTop: '30px' }}>
        <View className="button" style={{ textAlign: 'center' }}>
          搜索
        </View>
      </XList>
    </Layout>
  );
};
Search.config = {
  navigationBarTitleText: '搜索'
};
export default Search;
