import { View, Block } from '@tarojs/components';
import XRow from '../../../components/XRow';
import { formatHtml } from '../../../utils/utils';
import { resolutionObject } from '../../../constant/label';

const Historys = ({ actions, users }) => {
  const action = {
    opened: '创建',
    finished: '完成',
    closed: '关闭',
    deleted: '删除',
    started: '开始',
    createchildren: '创建子任务',
    edited: '编辑',
    commented: '添加备注',
    editfile: '编辑附件',
    recordestimate: '记录工时',
    canceled: '取消',
    assigned: '指派给',
    gitcommited: '提交代码',
    activated: '激活',
    confirmed: '确认需求变更'
  };

  const renderComment = (comment) =>
    comment && comment !== '<br />' ? (
      <View style="width: 100%;background: #f0f0f0;padding: 5px;margin-left: 15px;border: 1px solid #ccc;margin-top: 5px;overflow:auto;max-width:calc(100vw - 45px)">
        <wxparser rich-text={formatHtml(comment)} />
      </View>
    ) : (
      ''
    );

  const renderStrong = (str) => <View style="font-weight: 800;margin: 0 2px">{str}</View>;

  return (
    <View style={{ fontSize: '12px' }}>
      {actions.map((x, xIndex) => (
        <View key={x.id} style={{ display: 'flex', flexDirection: 'column', padding: '3px 0' }}>
          <View style={{ display: 'flex' }}>
            {xIndex + 1}. {x.date}，
            {x.action === 'recordestimate' ? (
              <Block>
                由{renderStrong(users[x.actor] || x.actor)}
                {action[x.action]}， 消耗 {renderStrong(x.extra)} 小时。
              </Block>
            ) : x.action === 'assigned' ? (
              <Block>
                由{renderStrong(users[x.actor] || x.actor)}
                {action[x.action]}
                {renderStrong(users[x.extra] || x.actor)}。
              </Block>
            ) : Object.keys(action).includes(x.action) ? (
              <Block>
                由{renderStrong(users[x.actor] || x.actor)}
                {action[x.action]}。
              </Block>
            ) : (
              <Block></Block>
            )}
          </View>
          {renderComment(x.comment || x.extra)}
        </View>
      ))}
    </View>
  );
};
Historys.config = {
  usingComponents: {
    wxparser: 'plugin://wxparserPlugin/wxparser'
  }
};
Historys.defaultProps = {
  actions: [],
  users: {}
};
export default Historys;
