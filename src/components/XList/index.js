import { View, Block } from '@tarojs/components';
import './index.less';
import icon_forward from '../../assets/page/icon_forward@2x.png';
import icon_drop_down from '../../assets/page/icon_drop_down@2x.png';
import XIcon from '../XIcon';

const XList = (props) => {
  const { title, ext, arrow, isCustomize = false, style = {}, isIndent = true, isMini } = props;
  const arrows = {
    right: icon_forward,
    bottom: icon_drop_down
  };
  const onClick = (e) => {
    if (typeof props.onClick === 'function' && !props.disabled) {
      props.onClick(e);
    }
  };
  const renderArrowIcon = arrow ? (
    <XIcon src={arrows[arrow]} style={{ width: '16px', height: '16px' }}></XIcon>
  ) : null;
  return (
    <View
      className={`list-view ${isMini ? 'list-view-mini' : ''}`}
      style={{ ...(!isIndent ? { paddingLeft: 0 } : {}), ...style }}
      onClick={onClick}
    >
      {isCustomize ? (
        <Block>
          <View className="list-view-icon">{props.renderIcon}</View>
          <View className="list-view-body">{props.children}</View>
          <View className="list-view-arrow">{renderArrowIcon}</View>
        </Block>
      ) : (
        <Block>
          <View className="list-view-icon">{props.renderIcon}</View>
          <View className="list-view-title">{title}</View>
          <View className="list-view-ext">{ext}</View>
          <View className="list-view-arrow">{renderArrowIcon}</View>
        </Block>
      )}
    </View>
  );
};

XList.defaultProps = {
  renderIcon: '',
  title: '',
  ext: '',
  arrow: '',
  isCustomize: false,
  disabled: false,
  style: {},
  isIndent: true,
  isMini: false,
  onClick: () => {}
};

export default XList;
