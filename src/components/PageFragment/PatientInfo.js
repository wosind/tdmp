import { View, Text, Button } from '@tarojs/components';
import { navigateTo } from '@tarojs/taro';
import { getAdmissionDays } from '../../utils/patient';

const PatientInfo = ({ patient, style, isShowAddAdviceLibs }) => {
  const onRedirect = () => {
    navigateTo({
      url:
        '/pages/workb/advice/group/edit?patientId=' + patient['id'] + '&hisId=' + patient['hisId']
    });
  };
  return (
    <View
      style={{
        borderBottom: '1px solid #DBDBDB',
        background: '#fff',
        padding: '10px 10px',
        ...style
      }}
    >
      <View style={{ display: 'flex', justifyContent: 'space-between' }}>
        <View>
          <Text style={{ fontWeight: 800, marginRight: '10px' }}>
            {patient['sickbedNo'] || ''}床
          </Text>
          <Text style={{ fontWeight: 800, marginRight: '10px' }}>{patient['name']}</Text>
          <Text style={{ marginRight: '10px' }}>{patient['sex']}</Text>
          <Text style={{ marginRight: '10px' }}>{patient['age']}</Text>
          <Text>入院{getAdmissionDays(patient)}天</Text>
        </View>
        {isShowAddAdviceLibs && (
          <View>
            <Button
              size="mini"
              style={{
                padding: '0 5px',
                background: '#fff',
                border: '1px solid #23d1a5',
                color: '#23d1a5'
              }}
              onClick={onRedirect}
            >
              添加病历库
            </Button>
          </View>
        )}
      </View>
      <View>诊断：{patient['diagnosis'] || ''}</View>
    </View>
  );
};
PatientInfo.defaultProps = {
  patient: {},
  style: {},
  isShowAddAdviceLibs: false
};
export default PatientInfo;
