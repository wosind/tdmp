import { View } from '@tarojs/components';
import XRow from '../XRow';

/**
 * 医嘱内容显示组件
 */
const AdviceContent = ({
  item: { doctoradviceType, contentAbstract, hisGroupOrderItemsEntities }
}) => {
  return doctoradviceType ? (
    doctoradviceType === 1 ? (
      (hisGroupOrderItemsEntities || []).map((k) => (
        <XRow key={k.item_NO}>
          <View style={{ flex: 1 }}>{k['drugName']}</View>
          <View style={{ fontSize: '14px', minWidth: '50px', textAlign: 'right' }}>
            {k['dosage'] || ''}
            {k['dosage_UNITS'] || ''}
          </View>
        </XRow>
      ))
    ) : (
      <View>{contentAbstract}</View>
    )
  ) : null;
};
AdviceContent.defaultProps = {
  item: {}
};
export default AdviceContent;
