import { View, Picker, Text } from '@tarojs/components';
import { useState } from '@tarojs/taro';
import XIcon from '../XIcon';
import icon_drop_down from '../../assets/page/icon_drop_down@2x.png';
import './index.less';

const XFilter = ({ filterOptions, onChange, style, valueStyle, value, showIcon, defaultLabel }) => {
  const [curr, setCurr] = useState({});
  const onChangeValue = (groupIndex, options, e) => {
    const index = e.detail.value;
    const value = options[index];
    setCurr({ ...curr, [groupIndex]: value });
    onChange(groupIndex, value, index);
  };
  return (
    <View className="xfilter-view" style={style}>
      {filterOptions.map((options, index) => (
        <Picker
          key={index}
          mode="selector"
          range={options}
          onChange={onChangeValue.bind(this, index, options)}
        >
          <View className="xfilter-view-label" style={valueStyle}>
            <Text className="xfilter-view-label-text">
              {curr[index] || options[value] || defaultLabel}
            </Text>
            {showIcon && (
              <XIcon src={icon_drop_down} style={{ width: '12px', height: '12px' }}></XIcon>
            )}
          </View>
        </Picker>
      ))}
    </View>
  );
};
XFilter.defaultProps = {
  filterOptions: [[]],
  style: {},
  valueStyle: {},
  value: 0,
  defaultLabel: '',
  showIcon: true,
  onChange: () => {}
};
export default XFilter;
