import { View } from '@tarojs/components';
import { useEffect, useState, useDidShow } from '@tarojs/taro';
import { getUserInfo } from '../../utils/utils';
import './index.less';

const Watermark = ({ title }) => {
  const [tag, setTag] = useState('');
  useDidShow(() => {
    const { name, departmentName } = getUserInfo();
    name && departmentName && setTag(`${name} ${departmentName}`);
  });
  useEffect(() => {
    title && setTag(title);
  }, [title]);
  return (
    <View className="watermark">
      {Array.from({ length: 15 }).map((x) => (
        <View key={x} className="watermark-item">
          {tag}
        </View>
      ))}
    </View>
  );
};
Watermark.defaultProps = {
  title: ''
};
export default Watermark;
