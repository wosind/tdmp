import { Image, View } from '@tarojs/components';
import './index.less';

const XIcon = ({ src, style }) => {
  return (
    <View className="xicon-view">
      <Image className="image" src={src} style={style}></Image>
    </View>
  );
};
XIcon.defaultProps = {
  src: null,
  style: { width: '24px', height: '24px', verticalAlign: 'middle' }
};
export default XIcon;
