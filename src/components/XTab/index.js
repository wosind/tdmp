import { Text, View } from '@tarojs/components';
import { useEffect, useState } from '@tarojs/taro';
import './index.less';

const XTab = ({ options, activeIndex, onChange, style, isRepeat }) => {
  const [curr, setCurr] = useState(0);
  useEffect(() => {
    setCurr(activeIndex);
  }, [activeIndex]);
  const onChangeValue = (x, index) => {
    setCurr(index);
    onChange(index);
  };
  return (
    <View className="xtab-view" style={style}>
      {options.map((x, xIndex) => (
        <Text
          key={x}
          className={`xtab-view-tab ${isRepeat ? 'xtab-view-repeat' : ''} ${
            curr === xIndex ? 'xtab-view-active' : ''
          }`}
          onClick={onChangeValue.bind(this, x, xIndex)}
        >
          {x}
        </Text>
      ))}
    </View>
  );
};
XTab.defaultProps = {
  options: [],
  activeIndex: 0,
  style: {},
  isRepeat: true,
  onChange: () => {}
};
export default XTab;
