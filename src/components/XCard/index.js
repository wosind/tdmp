import XList from '../XList';
import { View } from '@tarojs/components';
import './index.less';

const XCard = (props) => {
  const { isIndent, style, headerStyle, bodyStyle, footerStyle } = props;
  const onClick = (e) => {
    if (typeof props.onClick === 'function' && !props.disabled) {
      props.onClick(e);
    }
  };
  return (
    <View className="xcart-view" style={{ ...(isIndent ? { padding: '0 10px' } : {}), ...style }}>
      <View className="xcart-view-header" style={headerStyle}>
        {props.renderHeader}
      </View>
      <View className="xcart-view-body" style={bodyStyle} onClick={onClick}>
        {props.children}
      </View>
      <View className="xcart-view-footer" style={footerStyle}>
        {props.renderFooter}
      </View>
    </View>
  );
};
XCard.defaultProps = {
  renderHeader: null,
  renderFooter: null,
  isIndent: false,
  style: {},
  headerStyle: {},
  bodyStyle: {},
  footerStyle: {}
};
export default XCard;
