import { View, Button } from '@tarojs/components';
import XInput from '../XInput';
import { useState } from '@tarojs/taro';
import './index.less';

const XSearch = ({ style, placeholder, inputStyle, buttonStyle, onSearch }) => {
  const [key, setKey] = useState('');
  const [loading, setLoading] = useState(false);
  const onChangeSearch = (e) => {
    setKey(e.detail.value);
  };
  const onSearchValue = () => {
    setLoading(true);
    onSearch(key);
    setTimeout(() => {
      setLoading(false);
    }, 1000);
  };
  return (
    <View className="xsearch-view" style={style}>
      <View className="xsearch-view-input">
        <XInput
          placeholder={placeholder}
          inputStyle={{
            background: '#EFEFEF',
            borderRadius: '16px',
            fontSize: '14px',
            paddingTop: '5px',
            paddingBottom: '5px',
            ...inputStyle
          }}
          onChange={onChangeSearch}
        ></XInput>
      </View>
      <Button
        className="xsearch-view-button button"
        onClick={onSearchValue}
        style={buttonStyle}
        disabled={loading}
      >
        搜索
      </Button>
    </View>
  );
};
XSearch.defaultProps = {
  placeholder: '',
  style: {},
  inputStyle: {},
  buttonStyle: {},
  onSearch: () => {}
};
export default XSearch;
