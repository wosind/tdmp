import { View, Button } from '@tarojs/components';
import './index.less';
import { useState, useEffect } from '@tarojs/taro';

const XDialog = (props) => {
  const {
    title,
    style,
    headerStyle,
    bodyStyle,
    footerStyle,
    isCustomizeFooter,
    onClick,
    show
  } = props;
  const [display, setDisplay] = useState(false);
  useEffect(() => {
    setDisplay(show);
  }, [show]);
  const onClickFun = (vl) => {
    setDisplay(false);
    onClick(vl);
  };
  return display ? (
    <View className="xdialog-wrap">
      <View className="xdialog-mask"></View>
      <View className="xdialog" style={style}>
        <View className="xdialog-header" style={headerStyle}>
          {title}
        </View>
        <View className="xdialog-body" style={bodyStyle}>
          {props.children}
        </View>
        <View className="xdialog-footer" style={footerStyle}>
          {isCustomizeFooter ? (
            props.renderFooter
          ) : (
            <View className="xdialog-footer-inner">
              <Button
                className="xdialog-footer-inner-button"
                onClick={onClickFun.bind(this, { cancel: true })}
              >
                取消
              </Button>
              <Button
                className="xdialog-footer-inner-button"
                type="primary"
                onClick={onClickFun.bind(this, { confirm: true })}
              >
                确定
              </Button>
            </View>
          )}
        </View>
      </View>
    </View>
  ) : null;
};
XDialog.defaultProps = {
  show: false,
  renderFooter: null,
  style: {},
  headerStyle: {},
  bodyStyle: {},
  footerStyle: {},
  isCustomizeFooter: false,
  onClick: () => {}
};
export default XDialog;
