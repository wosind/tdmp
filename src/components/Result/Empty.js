import { Icon } from '@tarojs/components';
import Result from '.';

const Empty = ({ message }) => {
  return (
    <Result
      message={message}
      renderIcon={<Icon type="waiting" size="36" color="#bfbfbf"></Icon>}
    ></Result>
  );
};
Empty.defaultProps = {
  message: '暂无数据'
};
export default Empty;
