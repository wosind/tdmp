import { View } from '@tarojs/components';

const Result = (props) => {
  const { message, style = {} } = props;
  return (
    <View
      style={{
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center',
        padding: '30px 0',
        ...style
      }}
    >
      <View style={{ marginBottom: '12px' }}>{props.renderIcon}</View>
      <View>{message}</View>
    </View>
  );
};
Result.defaultProps = { message: '', style: {}, renderIcon: null };
export default Result;
