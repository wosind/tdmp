import './index.less';
import { View } from '@tarojs/components';
const XRow = (props) => {
  const { alignItems, justifyContent, style } = props;
  return (
    <View className="xrow" style={Object.assign(style, { alignItems, justifyContent })}>
      {props.children}
    </View>
  );
};
XRow.defaultProps = {
  alignItems: 'center',
  justifyContent: 'space-between',
  style: {}
};
export default XRow;
