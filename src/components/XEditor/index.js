import { View } from '@tarojs/components';
import { showModal } from '@tarojs/taro';

const Editor = ({ value, onChange, onCommit, width, height }) => {
  const onChangeValue = ({ detail }) => {
    //console.log('editor onChange', detail);
    onChange(detail);
  };
  const onCommitValue = ({ detail }) => {
    //console.log('editor onCommit', detail);
    showModal({ title: '操作提示', content: '确定编辑完成了吗' }).then(({ confirm }) => {
      if (confirm) {
        onCommit(detail);
      }
    });
  };
  return (
    <View
      style={{
        background: '#fff',
        zIndex: 20000,
        position: 'relative',
        height,
        width
      }}
    >
      <ec-editor onChange={onChangeValue} onCommit={onCommitValue} value={value}></ec-editor>
    </View>
  );
};
Editor.config = {
  usingComponents: {
    'ec-editor': './editor'
  }
};
Editor.defaultProps = {
  value: '',
  width: '100vw',
  height: '100vh',
  onChange: () => {},
  onCommit: () => {}
};
export default Editor;
