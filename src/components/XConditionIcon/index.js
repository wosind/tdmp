import { View } from '@tarojs/components';
import icon_bingzhong from '../../assets/page/icon_bingzhong@2x.png';
import icon_bingwi from '../../assets/page/icon_bingwi@2x.png';
import XIcon from '../XIcon';

const XConditionIcon = ({ condition = '' }) => {
  const conditionIcon = {
    病重: icon_bingzhong,
    病危: icon_bingwi
  };
  return (
    <View>
      <XIcon src={conditionIcon[condition]}></XIcon>
    </View>
  );
};

export default XConditionIcon;
