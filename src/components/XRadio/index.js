import XList from '../XList';
import { Radio, Label, RadioGroup } from '@tarojs/components';

const XRadio = ({ value, color, disabled, options, onChange }) => {
  const onChangeValue = (e) => {
    typeof onChange === 'function' && !disabled && onChange(e);
  };

  return (
    <XList isCustomize={true} isIndent={false}>
      {options.length && (
        <RadioGroup onChange={onChangeValue}>
          {options.map((x, xindex) => (
            <Label key={xindex}>
              <Radio
                value={x['value']}
                checked={value === x['value']}
                color={color}
                disabled={disabled}
                className="radio"
              >
                {x['text']}
              </Radio>
            </Label>
          ))}
        </RadioGroup>
      )}
    </XList>
  );
};
XRadio.defaultProps = {
  options: [],
  value: '',
  color: '',
  disabled: false,
  onChange: () => {}
};
export default XRadio;
