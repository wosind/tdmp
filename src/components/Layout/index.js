import { Block, Image, View } from '@tarojs/components';
import logo from '../../assets/page/HNJY.png';
import Watermark from '../Watermark';
import './index.less';

const Layout = (props) => {
  const { style, headerStyle, headerLogoStyle, isShowHeader, isFullScreen } = props;

  return (
    <View className="layout-view">
      <Watermark></Watermark>
      <View
        className="layout-view-inner"
        style={{ ...(isFullScreen ? {} : { padding: '0 10px' }), ...style }}
      >
        {isShowHeader ? (
          <View className="layout-view-inner-header" style={headerStyle}>
            <Image
              className="layout-view-inner-header-image"
              src={logo}
              style={headerLogoStyle}
              mode="widthFix"
            />
          </View>
        ) : null}
        <Block>{props.children}</Block>
      </View>
    </View>
  );
};

Layout.defaultProps = {
  style: {},
  headerStyle: {},
  headerLogoStyle: {
    width:'80%'
  },
  isShowHeader: false,
  isFullScreen: true
};

export default Layout;
