import Taro, { Component, showModal } from '@tarojs/taro';
import './app.scss';
import { CacheAppStartRouter } from './constant/cache';
import { setCache } from './utils/utils';

// 如果需要在 h5 环境中开启 React Devtools
// 取消以下注释：
// if (process.env.NODE_ENV !== 'production' && process.env.TARO_ENV === 'h5')  {
//   require('nerv-devtools')
// }

class App extends Component {
  componentDidMount() {}

  componentDidShow() {
    console.log('wxapp router', this.$router);
    setCache(CacheAppStartRouter, this.$router);
    this.onUpdateManager();
  }

  componentDidHide() {}

  componentDidCatchError() {}

  config = {
    pages: [
      'pages/auth/login',
      'pages/rd/index',
      'pages/rd/detail',
      'pages/rd/comment',
      'pages/rd/search',
      'pages/workb/index',
      'pages/workb/edit',
      'pages/workb/detail',
      'pages/workb/comment',
      'pages/workb/search',
      'pages/project/index',
      'pages/my/index',
      'pages/my/edit',
      'pages/my/webview',
      'pages/auth/index',
      'pages/auth/bio'
    ],
    window: {
      navigationBarBackgroundColor: '#fff',
      navigationBarTitleText: '蓝华ERP',
      navigationBarTextStyle: 'black',
      backgroundColor: '#f0f0f0',
      backgroundTextStyle: 'light',
      navigationStyle: 'default'
    },
    permission: {
      'scope.userLocation': {
        desc: '你的位置信息将用于小程序位置接口的效果展示'
      }
    },
    tabBar: {
      list: [
        {
          pagePath: 'pages/rd/index',
          text: '研发',
          iconPath: 'assets/tab/patients.png',
          selectedIconPath: 'assets/tab/patients2.png'
        },
        {
          pagePath: 'pages/workb/index',
          text: '运维',
          iconPath: 'assets/tab/workb.png',
          selectedIconPath: 'assets/tab/workb2.png'
        },
        {
          pagePath: 'pages/my/index',
          text: '个人中心',
          iconPath: 'assets/tab/my.png',
          selectedIconPath: 'assets/tab/my2.png'
        }
      ],
      color: '#333',
      selectedColor: '#00C2C1',
      backgroundColor: '#fff',
      borderStyle: 'black'
    },
    plugins: {
      wxparserPlugin: {
        version: '0.3.0',
        provider: 'wx9d4d4ffa781ff3ac'
      }
    }
  };

  onUpdateManager() {
    //更新
    if (!Taro.canIUse('getUpdateManager')) {
      return;
    }
    const updateManager = Taro.getUpdateManager();
    updateManager.onCheckForUpdate(function(res) {
      // 请求完新版本信息的回调
      if (res.hasUpdate) {
        console.log('wxapp has new version', res.hasUpdate);
      }
    });
    updateManager.onUpdateReady(function() {
      showModal({
        title: '有新版本更新',
        content: '新版本已经准备好，点击确定更新？',
        showCancel: false
      }).then(({ confirm }) => {
        if (confirm) {
          // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
          updateManager.applyUpdate();
        }
      });
    });

    updateManager.onUpdateFailed(function() {
      // 新的版本下载失败
      showModal({
        title: '有新版本更新',
        content: '新版本已经上线啦~，请您删除当前小程序，再打开'
      });
    });
  }

  // 在 App 类中的 render() 函数没有实际作用
  // 请勿修改此函数
  render() {
    return <Index />;
  }
}

Taro.render(<App />, document.getElementById('app'));
